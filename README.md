# TrebleCheck

## Canonical Source

Souce code is located at https://gitlab.com/hackintosh5/TrebleInfo

## Contributing

Code improvements are welcome! Just submit a merge request to this project.

## Translating

You can translate at https://poeditor.com/join/project/Ol1euLyZSr and https://poeditor.com/join/project/XE92nUJKqN. Please request to join both then [send me an email](mailto:treble@hack5.dev) so that I can approve you.

## Download

[Google Play](https://play.google.com/store/apps/details?id=tk.hack5.treblecheck)
or
[GitLab](https://gitlab.com/hackintosh5/TrebleInfo/-/releases)
